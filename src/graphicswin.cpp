//-----------------------------------------------------------------------------
// Top-level implementation of the program's main window, in which a graphical
// representation of the model is drawn and edited by the user.
//
// Copyright 2008-2013 Jonathan Westhues.
//-----------------------------------------------------------------------------
#include "solvespace.h"
#include "config.h"

#define mView (&GraphicsWindow::MenuView)
#define mEdit (&GraphicsWindow::MenuEdit)
#define mClip (&GraphicsWindow::MenuClipboard)
#define mReq  (&GraphicsWindow::MenuRequest)
#define mCon  (&Constraint::MenuConstrain)
#define mFile (&SolveSpaceUI::MenuFile)
#define mGrp  (&Group::MenuGroup)
#define mAna  (&SolveSpaceUI::MenuAnalyze)
#define mHelp (&SolveSpaceUI::MenuHelp)
#define DEL   DELETE_KEY
#define ESC   ESCAPE_KEY
#define S     SHIFT_MASK
#define C     CTRL_MASK
#define F(k)  (FUNCTION_KEY_BASE+(k))
#define TN    MENU_ITEM_NORMAL
#define TC    MENU_ITEM_CHECK
#define TR    MENU_ITEM_RADIO
const GraphicsWindow::MenuEntry GraphicsWindow::menu[] = {
//level
//   label                          id                  accel    ty   fn
{ 0, "文件 (&F)",                   0,                  0,       TN, NULL  },
{ 1, "新建 (&N)",                   MNU_NEW,            C|'N',   TN, mFile },
{ 1, "打开 (&O)",                   MNU_OPEN,           C|'O',   TN, mFile },
{ 1, "打开近期文件 (&R)",           MNU_OPEN_RECENT,    0,       TN, mFile },
{ 1, "保存 (&S)",                   MNU_SAVE,           C|'S',   TN, mFile },
{ 1, "另存为 (&A)",                 MNU_SAVE_AS,        0,       TN, mFile },
{ 1,  NULL,                         0,                  0,       TN, NULL  },
{ 1, "导入格式 (&P)",               MNU_IMPORT         ,0,       TN, mFile },
{ 1,  NULL,                         0,                  0,       TN, NULL  },
{ 1, "导出图片 (&I)",               MNU_EXPORT_PNG,     0,       TN, mFile },
{ 1, "导出视图 (&V)",               MNU_EXPORT_VIEW,    0,       TN, mFile },
{ 1, "导出截面图 (&S)",             MNU_EXPORT_SECTION, 0,       TN, mFile },
{ 1, "导出线框图 (&W)",             MNU_EXPORT_WIREFRAME, 0,     TN, mFile },
{ 1, "导出模型 (&M)",               MNU_EXPORT_MESH,    0,       TN, mFile },
{ 1, "导出表面 (&S)",               MNU_EXPORT_SURFACES,0,       TN, mFile },
#ifndef __APPLE__
{ 1,  NULL,                         0,                  0,       TN, NULL  },
{ 1, "退出 (&X)",                   MNU_EXIT,           C|'Q',   TN, mFile },
#endif

{ 0, "编辑 (&E)",                   0,                  0,       TN, NULL  },
{ 1, "撤销 (&U)",                   MNU_UNDO,           C|'Z',   TN, mEdit },
{ 1, "重做 (&R)",                   MNU_REDO,           C|'Y',   TN, mEdit },
{ 1, "重新生成全部 (&G)",           MNU_REGEN_ALL,      ' ',     TN, mEdit },
{ 1,  NULL,                         0,                  0,       TN, NULL  },
{ 1, "对齐到网格 (&G)",             MNU_SNAP_TO_GRID,   '.',     TN, mEdit },
{ 1, "将导入旋转90度 (&9)",         MNU_ROTATE_90,      '9',     TN, mEdit },
{ 1,  NULL,                         0,                  0,       TN, NULL  },
{ 1, "剪切 (&T)",                   MNU_CUT,            C|'X',   TN, mClip },
{ 1, "拷贝 (&C)",                   MNU_COPY,           C|'C',   TN, mClip },
{ 1, "粘贴 (&P)",                   MNU_PASTE,          C|'V',   TN, mClip },
{ 1, "参数化粘贴 (&T)",             MNU_PASTE_TRANSFORM,C|'T',   TN, mClip },
{ 1, "删除 (&D)",                   MNU_DELETE,         DEL,     TN, mClip },
{ 1,  NULL,                         0,                  0,       TN, NULL  },
{ 1, "选择边缘链 (&E)",             MNU_SELECT_CHAIN,   C|'E',   TN, mEdit },
{ 1, "选择全部 (&A)",               MNU_SELECT_ALL,     C|'A',   TN, mEdit },
{ 1, "取消选择 (&U)",               MNU_UNSELECT_ALL,   ESC,     TN, mEdit },
{ 1,  NULL,                         0,                  0,       TN, NULL  },
{ 1, "线型管理(&L)",                MNU_EDIT_LINE_STYLES,0,      TN, mEdit },
{ 1, "显示参数(&V)",                MNU_VIEW_PROJECTION,0,       TN, mEdit },
#ifndef __APPLE__
{ 1, "系统设置(&F)",                MNU_CONFIGURATION,  0,       TN, mEdit  },
#endif

{ 0, "查看 (&V)",                   0,                  0,       TN, NULL  },
{ 1, "放大 (&I)",                   MNU_ZOOM_IN,        '+',     TN, mView },
{ 1, "缩小 (&O)",                   MNU_ZOOM_OUT,       '-',     TN, mView },
{ 1, "缩放到适合 (&F)",             MNU_ZOOM_TO_FIT,    'F',     TN, mView },
{ 1,  NULL,                         0,                  0,       TN, NULL  },
{ 1, "对齐工作面 (&W)",             MNU_ONTO_WORKPLANE, 'W',     TN, mView },
{ 1, "正交视图 (&O)",               MNU_NEAREST_ORTHO,  F(2),    TN, mView },
{ 1, "等轴视图 (&I)",               MNU_NEAREST_ISO,    F(3),    TN, mView },
{ 1, "中心视图 (&C)",               MNU_CENTER_VIEW,    F(4),    TN, mView },
{ 1,  NULL,                         0,                  0,       TN, NULL  },
{ 1, "显示网格 (&G)",               MNU_SHOW_GRID,      '>',     TC, mView },
{ 1, "使用透视投影 (&P)",           MNU_PERSPECTIVE_PROJ,'`',    TC, mView },
{ 1,  NULL,                         0,                  0,       TN, NULL  },
#if defined(__APPLE__)
{ 1, "Show Menu &Bar",              MNU_SHOW_MENU_BAR,  C|F(12), TC, mView },
#endif
{ 1, "显示工具栏 (&T)",             MNU_SHOW_TOOLBAR,   0,       TC, mView },
{ 1, "显示属性面板 (&W)",           MNU_SHOW_TEXT_WND,  '\t',    TC, mView },
{ 1,  NULL,                         0,                  0,       TN, NULL  },
{ 1, "使用英制单位 (&I)",           MNU_UNITS_INCHES,   0,       TR, mView },
{ 1, "使用公制单位 (&M)",           MNU_UNITS_MM,       0,       TR, mView },
{ 1,  NULL,                         0,                  0,       TN, NULL  },
{ 1, "全屏显示 (&F)",               MNU_FULL_SCREEN,    C|F(11), TC, mView },

{ 0, "新建组 (&N)",                 0,                  0,       TN, NULL  },
{ 1, "新建草图（所选平面）(&W)",    MNU_GROUP_WRKPL,    S|'W',   TN, mGrp  },
{ 1, "新建草图（当前视图）(&3)",    MNU_GROUP_3D,       S|'3',   TN, mGrp  },
{ 1, NULL,                          0,                  0,       TN, NULL  },
{ 1, "位移阵列 (&T)",               MNU_GROUP_TRANS,    S|'T',   TN, mGrp  },
{ 1, "旋转阵列 (&R)",               MNU_GROUP_ROT,      S|'R',   TN, mGrp  },
{ 1, NULL,                          0,                  0,       TN, NULL  },
{ 1, "拉伸 (&X)",                   MNU_GROUP_EXTRUDE,  S|'X',   TN, mGrp  },
{ 1, "旋转 (&L)",                   MNU_GROUP_LATHE,    S|'L',   TN, mGrp  },
{ 1, NULL,                          0,                  0,       TN, NULL  },
{ 1, "组装零件 (&I)",               MNU_GROUP_LINK,     S|'I',   TN, mGrp  },
{ 1, "近期零件",                    MNU_GROUP_RECENT,   0,       TN, mGrp  },

{ 0, "草图 (&S)",                   0,                  0,       TN, NULL  },
{ 1, "平面草图 (&2)",               MNU_SEL_WORKPLANE,  '2',     TR, mReq  },
{ 1, "三维草图 (&3)",               MNU_FREE_IN_3D,     '3',     TR, mReq  },
{ 1, NULL,                          0,                  0,       TN, NULL  },
{ 1, "基准点 (&P)",                 MNU_DATUM_POINT,    'P',     TN, mReq  },
{ 1, "工作面 (&W)",                 MNU_WORKPLANE,      0,       TN, mReq  },
{ 1, NULL,                          0,                  0,       TN, NULL  },
{ 1, "线段 (&S)",                   MNU_LINE_SEGMENT,   'S',     TN, mReq  },
{ 1, "构造线 (&O)",                 MNU_CONSTR_SEGMENT, S|'S',   TN, mReq  },
{ 1, "矩形 (&R)",                   MNU_RECTANGLE,      'R',     TN, mReq  },
{ 1, "圆形 (&C)",                   MNU_CIRCLE,         'C',     TN, mReq  },
{ 1, "圆弧 (&A)",                   MNU_ARC,            'A',     TN, mReq  },
{ 1, "曲线 (&B)",                   MNU_CUBIC,          'B',     TN, mReq  },
{ 1, NULL,                          0,                  0,       TN, NULL  },
{ 1, "文字 (&T)",                   MNU_TTF_TEXT,       'T',     TN, mReq  },
{ 1, NULL,                          0,                  0,       TN, NULL  },
{ 1, "转换为构造线 (&G)",           MNU_CONSTRUCTION,   'G',     TN, mReq  },
{ 1, "圆角过渡 (&A)",               MNU_TANGENT_ARC,    S|'A',   TN, mReq  },
{ 1, "打断 (&I)",                   MNU_SPLIT_CURVES,   'I',     TN, mReq  },

{ 0, "约束 (&C)",                   0,                  0,       TN, NULL  },
{ 1, "尺寸 (&D)",                   MNU_DISTANCE_DIA,   'D',     TN, mCon  },
{ 1, "参考尺寸 (&F)",               MNU_REF_DISTANCE,   S|'D',   TN, mCon  },
{ 1, "角度 (&N)",                   MNU_ANGLE,          'N',     TN, mCon  },
{ 1, "参考角度 (&G)",               MNU_REF_ANGLE,      S|'N',   TN, mCon  },
{ 1, "切换内外角 (&U)",             MNU_OTHER_ANGLE,    'U',     TN, mCon  },
{ 1, "转换为参考 (&E)",             MNU_REFERENCE,      'E',     TN, mCon  },
{ 1, NULL,                          0,                  0,       TN, NULL  },
{ 1, "水平 (&H)",                   MNU_HORIZONTAL,     'H',     TN, mCon  },
{ 1, "垂直 (&V)",                   MNU_VERTICAL,       'V',     TN, mCon  },
{ 1, NULL,                          0,                  0,       TN, NULL  },
{ 1, "相合 (&O)",                   MNU_ON_ENTITY,      'O',     TN, mCon  },
{ 1, "相等 (&Q)",                   MNU_EQUAL,          'Q',     TN, mCon  },
{ 1, "等比 (&T)",                   MNU_RATIO,          'Z',     TN, mCon  },
{ 1, "等差 (&E)",                   MNU_DIFFERENCE,     'J',     TN, mCon  },
{ 1, "中点 (&M)",                   MNU_AT_MIDPOINT,    'M',     TN, mCon  },
{ 1, "对称 (&Y)",                   MNU_SYMMETRIC,      'Y',     TN, mCon  },
{ 1, "平行 / 相切 (&L)",            MNU_PARALLEL,       'L',     TN, mCon  },
{ 1, "垂直 (&P)",                   MNU_PERPENDICULAR,  '[',     TN, mCon  },
{ 1, "同向 (&A)",                   MNU_ORIENTED_SAME,  'X',     TN, mCon  },
{ 1, "锁定位置 (&D)",               MNU_WHERE_DRAGGED,  ']',     TN, mCon  },
{ 1, NULL,                          0,                  0,       TN, NULL  },
{ 1, "注释",                        MNU_COMMENT,        ';',     TN, mCon  },

{ 0, "分析 (&A)",                   0,                  0,       TN, NULL  },
{ 1, "测量体积 (&V)",               MNU_VOLUME,         C|S|'V', TN, mAna  },
{ 1, "测量面积 (&A)",               MNU_AREA,           C|S|'A', TN, mAna  },
{ 1, "显示干涉 (&I)",               MNU_INTERFERENCE,   C|S|'I', TN, mAna  },
{ 1, "显示裸线 (&N)",               MNU_NAKED_EDGES,    C|S|'N', TN, mAna  },
{ 1, "显示重心 (&C)",               MNU_CENTER_OF_MASS, C|S|'C', TN, mAna  },
{ 1, NULL,                          0,                  0,       TN, NULL  },
{ 1, "显示自由度 (&F)",             MNU_SHOW_DOF,       C|S|'F', TN, mAna  },
{ 1, NULL,                          0,                  0,       TN, NULL  },
{ 1, "追踪轨迹 (&T)",               MNU_TRACE_PT,       C|S|'T', TN, mAna  },
{ 1, "停止追踪 (&S)",               MNU_STOP_TRACING,   C|S|'S', TN, mAna  },
{ 1, "尺寸步进 (&D)",               MNU_STEP_DIM,       C|S|'D', TN, mAna  },

{ 0, "帮助 (&H)",                   0,                  0,       TN, NULL  },
{ 1, "手册 (&W)",                   MNU_WEBSITE,        0,       TN, mHelp },
#ifndef __APPLE__
{ 1, "关于 (&A)",                   MNU_ABOUT,          0,       TN, mHelp },
#endif

{ -1, 0, 0, 0, TN, 0 }
};

#undef DEL
#undef ESC
#undef S
#undef C
#undef F
#undef TN
#undef TC
#undef TR

std::string SolveSpace::MakeAcceleratorLabel(int accel) {
    if(!accel) return "";

    std::string label;
    if(accel & GraphicsWindow::CTRL_MASK) {
        label += "Ctrl+";
        accel ^= GraphicsWindow::CTRL_MASK;
    }
    if(accel & GraphicsWindow::SHIFT_MASK) {
        label += "Shift+";
        accel ^= GraphicsWindow::SHIFT_MASK;
    }
    if(accel >= GraphicsWindow::FUNCTION_KEY_BASE + 1 &&
       accel <= GraphicsWindow::FUNCTION_KEY_BASE + 12) {
        label += ssprintf("F%d", accel - GraphicsWindow::FUNCTION_KEY_BASE);
    } else if(accel == '\t') {
        label += "Tab";
    } else if(accel == ' ') {
        label += "Space";
    } else if(accel == GraphicsWindow::ESCAPE_KEY) {
        label += "Esc";
    } else if(accel == GraphicsWindow::DELETE_KEY) {
        label += "Del";
    } else {
        label += (char)(accel & 0xff);
    }
    return label;
}

void GraphicsWindow::Init(void) {
    scale = 5;
    offset    = Vector::From(0, 0, 0);
    projRight = Vector::From(1, 0, 0);
    projUp    = Vector::From(0, 1, 0);

    // Make sure those are valid; could get a mouse move without a mouse
    // down if someone depresses the button, then drags into our window.
    orig.projRight = projRight;
    orig.projUp = projUp;

    // And with the last group active
    activeGroup = SK.groupOrder.elem[SK.groupOrder.n - 1];
    SK.GetGroup(activeGroup)->Activate();

    showWorkplanes = false;
    showNormals = true;
    showPoints = true;
    showConstraints = true;
    showHdnLines = false;
    showShaded = true;
    showEdges = true;
    showMesh = false;
    showOutlines = false;

    showTextWindow = true;
    ShowTextWindow(showTextWindow);

    showSnapGrid = false;
    context.active = false;
    toolbarHovered = 0;

    // Do this last, so that all the menus get updated correctly.
    ClearSuper();
}

void GraphicsWindow::AnimateOntoWorkplane(void) {
    if(!LockedInWorkplane()) return;

    Entity *w = SK.GetEntity(ActiveWorkplane());
    Quaternion quatf = w->Normal()->NormalGetNum();

    // Get Z pointing vertical, if we're on turntable nav mode:
    if(SS.turntableNav) {
        Vector normalRight = quatf.RotationU();
        Vector normalUp    = quatf.RotationV();
        Vector normal      = normalRight.Cross(normalUp);
        if(normalRight.z != 0) {
            double theta = atan2(normalUp.z, normalRight.z);
            theta -= atan2(1, 0);
            normalRight = normalRight.RotatedAbout(normal, theta);
            normalUp    = normalUp.RotatedAbout(normal, theta);
            quatf       = Quaternion::From(normalRight, normalUp);
        }
    }
    
    Vector offsetf = (SK.GetEntity(w->point[0])->PointGetNum()).ScaledBy(-1);

    AnimateOnto(quatf, offsetf);
}

void GraphicsWindow::AnimateOnto(Quaternion quatf, Vector offsetf) {
    // Get our initial orientation and translation.
    Quaternion quat0 = Quaternion::From(projRight, projUp);
    Vector offset0 = offset;

    // Make sure we take the shorter of the two possible paths.
    double mp = (quatf.Minus(quat0)).Magnitude();
    double mm = (quatf.Plus(quat0)).Magnitude();
    if(mp > mm) {
        quatf = quatf.ScaledBy(-1);
        mp = mm;
    }
    double mo = (offset0.Minus(offsetf)).Magnitude()*scale;

    // Animate transition, unless it's a tiny move.
    int32_t dt = (mp < 0.01 && mo < 10) ? (-20) :
                     (int32_t)(100 + 1000*mp + 0.4*mo);
    // Don't ever animate for longer than 2000 ms; we can get absurdly
    // long translations (as measured in pixels) if the user zooms out, moves,
    // and then zooms in again.
    if(dt > 2000) dt = 2000;
    int64_t tn, t0 = GetMilliseconds();
    double s = 0;
    Quaternion dq = quatf.Times(quat0.Inverse());
    do {
        offset = (offset0.ScaledBy(1 - s)).Plus(offsetf.ScaledBy(s));
        Quaternion quat = (dq.ToThe(s)).Times(quat0);
        quat = quat.WithMagnitude(1);

        projRight = quat.RotationU();
        projUp    = quat.RotationV();
        PaintGraphics();

        tn = GetMilliseconds();
        s = (tn - t0)/((double)dt);
    } while((tn - t0) < dt);

    projRight = quatf.RotationU();
    projUp = quatf.RotationV();
    offset = offsetf;
    InvalidateGraphics();
    // If the view screen is open, then we need to refresh it.
    SS.ScheduleShowTW();
}

void GraphicsWindow::HandlePointForZoomToFit(Vector p, Point2d *pmax, Point2d *pmin,
                                             double *wmin, bool usePerspective)
{
    double w;
    Vector pp = ProjectPoint4(p, &w);
    // If usePerspective is true, then we calculate a perspective projection of the point.
    // If not, then we do a parallel projection regardless of the current
    // scale factor.
    if(usePerspective) {
        pp = pp.ScaledBy(1.0/w);
    }

    pmax->x = max(pmax->x, pp.x);
    pmax->y = max(pmax->y, pp.y);
    pmin->x = min(pmin->x, pp.x);
    pmin->y = min(pmin->y, pp.y);
    *wmin = min(*wmin, w);
}
void GraphicsWindow::LoopOverPoints(const std::vector<Entity *> &entities,
                                    const std::vector<hEntity> &faces,
                                    Point2d *pmax, Point2d *pmin, double *wmin,
                                    bool usePerspective, bool includeMesh) {

    int i, j;
    for(Entity *e : entities) {
        if(e->IsPoint()) {
            HandlePointForZoomToFit(e->PointGetNum(), pmax, pmin, wmin, usePerspective);
        } else if(e->type == Entity::CIRCLE) {
            // Lots of entities can extend outside the bbox of their points,
            // but circles are particularly bad. We want to get things halfway
            // reasonable without the mesh, because a zoom to fit is used to
            // set the zoom level to set the chord tol.
            double r = e->CircleGetRadiusNum();
            Vector c = SK.GetEntity(e->point[0])->PointGetNum();
            Quaternion q = SK.GetEntity(e->normal)->NormalGetNum();
            for(j = 0; j < 4; j++) {
                Vector p = (j == 0) ? (c.Plus(q.RotationU().ScaledBy( r))) :
                           (j == 1) ? (c.Plus(q.RotationU().ScaledBy(-r))) :
                           (j == 2) ? (c.Plus(q.RotationV().ScaledBy( r))) :
                                      (c.Plus(q.RotationV().ScaledBy(-r)));
                HandlePointForZoomToFit(p, pmax, pmin, wmin, usePerspective);
            }
        } else {
            // We have to iterate children points, because we can select entites without points
            for(int i = 0; i < MAX_POINTS_IN_ENTITY; i++) {
                if(e->point[i].v == 0) break;
                Vector p = SK.GetEntity(e->point[i])->PointGetNum();
                HandlePointForZoomToFit(p, pmax, pmin, wmin, usePerspective);
            }
        }
    }

    if(!includeMesh && faces.empty()) return;

    Group *g = SK.GetGroup(activeGroup);
    g->GenerateDisplayItems();
    for(i = 0; i < g->displayMesh.l.n; i++) {
        STriangle *tr = &(g->displayMesh.l.elem[i]);
        if(!includeMesh) {
            bool found = false;
            for(const hEntity &face : faces) {
                if(face.v != tr->meta.face) continue;
                found = true;
                break;
            }
            if(!found) continue;
        }
        HandlePointForZoomToFit(tr->a, pmax, pmin, wmin, usePerspective);
        HandlePointForZoomToFit(tr->b, pmax, pmin, wmin, usePerspective);
        HandlePointForZoomToFit(tr->c, pmax, pmin, wmin, usePerspective);
    }
    if(!includeMesh) return;
    for(i = 0; i < g->polyLoops.l.n; i++) {
        SContour *sc = &(g->polyLoops.l.elem[i]);
        for(j = 0; j < sc->l.n; j++) {
            HandlePointForZoomToFit(sc->l.elem[j].p, pmax, pmin, wmin, usePerspective);
        }
    }
}
void GraphicsWindow::ZoomToFit(bool includingInvisibles, bool useSelection) {
    std::vector<Entity *> entities;
    std::vector<hEntity> faces;

    if(useSelection) {
        for(int i = 0; i < selection.n; i++) {
            Selection *s = &selection.elem[i];
            if(s->entity.v == 0) continue;
            Entity *e = SK.entity.FindById(s->entity);
            if(e->IsFace()) {
                faces.push_back(e->h);
                continue;
            }
            entities.push_back(e);
        }
    }

    bool selectionUsed = !entities.empty() || !faces.empty();

    if(!selectionUsed) {
        for(int i = 0; i<SK.entity.n; i++) {
            Entity *e = &(SK.entity.elem[i]);
            // we don't want to handle separate points, because we will iterate them inside entities.
            if(e->IsPoint()) continue;
            if(!includingInvisibles && !e->IsVisible()) continue;
            entities.push_back(e);
        }
    }

    // On the first run, ignore perspective.
    Point2d pmax = { -1e12, -1e12 }, pmin = { 1e12, 1e12 };
    double wmin = 1;
    LoopOverPoints(entities, faces, &pmax, &pmin, &wmin,
                   /*usePerspective=*/false, /*includeMesh=*/!selectionUsed);

    double xm = (pmax.x + pmin.x)/2, ym = (pmax.y + pmin.y)/2;
    double dx = pmax.x - pmin.x, dy = pmax.y - pmin.y;

    offset = offset.Plus(projRight.ScaledBy(-xm)).Plus(
                         projUp.   ScaledBy(-ym));

    // And based on this, we calculate the scale and offset
    if(EXACT(dx == 0 && dy == 0)) {
        scale = 5;
    } else {
        double scalex = 1e12, scaley = 1e12;
        if(EXACT(dx != 0)) scalex = 0.9*width /dx;
        if(EXACT(dy != 0)) scaley = 0.9*height/dy;
        scale = min(scalex, scaley);

        scale = min(300.0, scale);
        scale = max(0.003, scale);
    }

    // Then do another run, considering the perspective.
    pmax.x = -1e12; pmax.y = -1e12;
    pmin.x =  1e12; pmin.y =  1e12;
    wmin = 1;
    LoopOverPoints(entities, faces, &pmax, &pmin, &wmin,
                   /*usePerspective=*/true, /*includeMesh=*/!selectionUsed);

    // Adjust the scale so that no points are behind the camera
    if(wmin < 0.1) {
        double k = SS.CameraTangent();
        // w = 1+k*scale*z
        double zmin = (wmin - 1)/(k*scale);
        // 0.1 = 1 + k*scale*zmin
        // (0.1 - 1)/(k*zmin) = scale
        scale = min(scale, (0.1 - 1)/(k*zmin));
    }
}

void GraphicsWindow::MenuView(int id) {
    switch(id) {
        case MNU_ZOOM_IN:
            SS.GW.scale *= 1.2;
            SS.ScheduleShowTW();
            break;

        case MNU_ZOOM_OUT:
            SS.GW.scale /= 1.2;
            SS.ScheduleShowTW();
            break;

        case MNU_ZOOM_TO_FIT:
            SS.GW.ZoomToFit(/*includingInvisibles=*/false, /*useSelection=*/true);
            SS.ScheduleShowTW();
            break;

        case MNU_SHOW_GRID:
            SS.GW.showSnapGrid = !SS.GW.showSnapGrid;
            if(SS.GW.showSnapGrid && !SS.GW.LockedInWorkplane()) {
                Message("没有任何活动状态的工作面，因此不会显示栅格。");
            }
            SS.GW.EnsureValidActives();
            InvalidateGraphics();
            break;

        case MNU_PERSPECTIVE_PROJ:
            SS.usePerspectiveProj = !SS.usePerspectiveProj;
            if(SS.cameraTangent < 1e-6) {
                Error("透视系数设置为零，因此视图将始终是平行投影。\n\n"
                      "对于透视投影，请在配置屏幕中修改透视系数。\n\n"
                      "通常值在0.3左右。");
            }
            SS.GW.EnsureValidActives();
            InvalidateGraphics();
            break;

        case MNU_ONTO_WORKPLANE:
            if(SS.GW.LockedInWorkplane()) {
                SS.GW.AnimateOntoWorkplane();
                SS.ScheduleShowTW();
                break;
            }  // if not in 2d mode fall through and use ORTHO logic
        case MNU_NEAREST_ORTHO:
        case MNU_NEAREST_ISO: {
            static const Vector ortho[3] = {
                Vector::From(1, 0, 0),
                Vector::From(0, 1, 0),
                Vector::From(0, 0, 1)
            };
            double sqrt2 = sqrt(2.0), sqrt6 = sqrt(6.0);
            Quaternion quat0 = Quaternion::From(SS.GW.projRight, SS.GW.projUp);
            Quaternion quatf = quat0;
            double dmin = 1e10;

            // There are 24 possible views; 3*2*2*2
            int i, j, negi, negj;
            for(i = 0; i < 3; i++) {
                for(j = 0; j < 3; j++) {
                    if(i == j) continue;
                    for(negi = 0; negi < 2; negi++) {
                        for(negj = 0; negj < 2; negj++) {
                            Vector ou = ortho[i], ov = ortho[j];
                            if(negi) ou = ou.ScaledBy(-1);
                            if(negj) ov = ov.ScaledBy(-1);
                            Vector on = ou.Cross(ov);

                            Vector u, v;
                            if(id == MNU_NEAREST_ORTHO || id == MNU_ONTO_WORKPLANE) {
                                u = ou;
                                v = ov;
                            } else {
                                u =
                                    ou.ScaledBy(1/sqrt2).Plus(
                                    on.ScaledBy(-1/sqrt2));
                                v =
                                    ou.ScaledBy(-1/sqrt6).Plus(
                                    ov.ScaledBy(2/sqrt6).Plus(
                                    on.ScaledBy(-1/sqrt6)));
                            }

                            Quaternion quatt = Quaternion::From(u, v);
                            double d = min(
                                (quatt.Minus(quat0)).Magnitude(),
                                (quatt.Plus(quat0)).Magnitude());
                            if(d < dmin) {
                                dmin = d;
                                quatf = quatt;
                            }
                        }
                    }
                }
            }

            SS.GW.AnimateOnto(quatf, SS.GW.offset);
            break;
        }

        case MNU_CENTER_VIEW:
            SS.GW.GroupSelection();
            if(SS.GW.gs.n == 1 && SS.GW.gs.points == 1) {
                Quaternion quat0;
                // Offset is the selected point, quaternion is same as before
                Vector pt = SK.GetEntity(SS.GW.gs.point[0])->PointGetNum();
                quat0 = Quaternion::From(SS.GW.projRight, SS.GW.projUp);
                SS.GW.AnimateOnto(quat0, pt.ScaledBy(-1));
                SS.GW.ClearSelection();
            } else {
                //Error("请选择一个点作为新的显示中心。");
                SS.UpdateCenterOfMass();
                SS.centerOfMass.draw = false;

                Quaternion quat0;
                // Offset is the selected point, quaternion is same as before
                Vector pt = SS.centerOfMass.position;
                quat0 = Quaternion::From(SS.GW.projRight, SS.GW.projUp);
                SS.GW.AnimateOnto(quat0, pt.ScaledBy(-1));
                SS.GW.ClearSelection();
            }
            break;

        case MNU_SHOW_MENU_BAR:
            ToggleMenuBar();
            SS.GW.EnsureValidActives();
            InvalidateGraphics();
            break;

        case MNU_SHOW_TOOLBAR:
            SS.showToolbar = !SS.showToolbar;
            SS.GW.EnsureValidActives();
            InvalidateGraphics();
            break;

        case MNU_SHOW_TEXT_WND:
            SS.GW.showTextWindow = !SS.GW.showTextWindow;
            SS.GW.EnsureValidActives();
            break;

        case MNU_UNITS_INCHES:
            SS.viewUnits = SolveSpaceUI::UNIT_INCHES;
            SS.ScheduleShowTW();
            SS.GW.EnsureValidActives();
            break;

        case MNU_UNITS_MM:
            SS.viewUnits = SolveSpaceUI::UNIT_MM;
            SS.ScheduleShowTW();
            SS.GW.EnsureValidActives();
            break;

        case MNU_FULL_SCREEN:
            ToggleFullScreen();
            SS.GW.EnsureValidActives();
            break;

        default: oops();
    }
    InvalidateGraphics();
}

void GraphicsWindow::EnsureValidActives(void) {
    bool change = false;
    // The active group must exist, and not be the references.
    Group *g = SK.group.FindByIdNoOops(activeGroup);
    if((!g) || (g->h.v == Group::HGROUP_REFERENCES.v)) {
        int i;
        for(i = 0; i < SK.groupOrder.n; i++) {
            if(SK.groupOrder.elem[i].v != Group::HGROUP_REFERENCES.v) {
                break;
            }
        }
        if(i >= SK.groupOrder.n) {
            // This can happen if the user deletes all the groups in the
            // sketch. It's difficult to prevent that, because the last
            // group might have been deleted automatically, because it failed
            // a dependency. There needs to be something, so create a plane
            // drawing group and activate that. They should never be able
            // to delete the references, though.
            activeGroup = SS.CreateDefaultDrawingGroup();
            // We've created the default group, but not the workplane entity;
            // do it now so that drawing mode isn't switched to "Free in 3d".
            SS.GenerateAll(SolveSpaceUI::GENERATE_ALL);
        } else {
            activeGroup = SK.groupOrder.elem[i];
        }
        SK.GetGroup(activeGroup)->Activate();
        change = true;
    }

    // The active coordinate system must also exist.
    if(LockedInWorkplane()) {
        Entity *e = SK.entity.FindByIdNoOops(ActiveWorkplane());
        if(e) {
            hGroup hgw = e->group;
            if(hgw.v != activeGroup.v && SS.GroupsInOrder(activeGroup, hgw)) {
                // The active workplane is in a group that comes after the
                // active group; so any request or constraint will fail.
                SetWorkplaneFreeIn3d();
                change = true;
            }
        } else {
            SetWorkplaneFreeIn3d();
            change = true;
        }
    }

    // And update the checked state for various menus
    bool locked = LockedInWorkplane();
    RadioMenuById(MNU_FREE_IN_3D, !locked);
    RadioMenuById(MNU_SEL_WORKPLANE, locked);

    SS.UndoEnableMenus();

    switch(SS.viewUnits) {
        case SolveSpaceUI::UNIT_MM:
        case SolveSpaceUI::UNIT_INCHES:
            break;
        default:
            SS.viewUnits = SolveSpaceUI::UNIT_MM;
            break;
    }
    RadioMenuById(MNU_UNITS_MM, SS.viewUnits == SolveSpaceUI::UNIT_MM);
    RadioMenuById(MNU_UNITS_INCHES, SS.viewUnits == SolveSpaceUI::UNIT_INCHES);

    ShowTextWindow(SS.GW.showTextWindow);
    CheckMenuById(MNU_SHOW_TEXT_WND, SS.GW.showTextWindow);

#if defined(__APPLE__)
    CheckMenuById(MNU_SHOW_MENU_BAR, MenuBarIsVisible());
#endif
    CheckMenuById(MNU_SHOW_TOOLBAR, SS.showToolbar);
    CheckMenuById(MNU_PERSPECTIVE_PROJ, SS.usePerspectiveProj);
    CheckMenuById(MNU_SHOW_GRID, SS.GW.showSnapGrid);
    CheckMenuById(MNU_FULL_SCREEN, !FullScreenIsActive());

    if(change) SS.ScheduleShowTW();
}

void GraphicsWindow::SetWorkplaneFreeIn3d(void) {
    SK.GetGroup(activeGroup)->activeWorkplane = Entity::FREE_IN_3D;
}
hEntity GraphicsWindow::ActiveWorkplane(void) {
    Group *g = SK.group.FindByIdNoOops(activeGroup);
    if(g) {
        return g->activeWorkplane;
    } else {
        return Entity::FREE_IN_3D;
    }
}
bool GraphicsWindow::LockedInWorkplane(void) {
    return (SS.GW.ActiveWorkplane().v != Entity::FREE_IN_3D.v);
}

void GraphicsWindow::ForceTextWindowShown(void) {
    if(!showTextWindow) {
        showTextWindow = true;
        CheckMenuById(MNU_SHOW_TEXT_WND, true);
        ShowTextWindow(true);
    }
}

void GraphicsWindow::DeleteTaggedRequests(void) {
    // Rewrite any point-coincident constraints that were affected by this
    // deletion.
    Request *r;
    for(r = SK.request.First(); r; r = SK.request.NextAfter(r)) {
        if(!r->tag) continue;
        FixConstraintsForRequestBeingDeleted(r->h);
    }
    // and then delete the tagged requests.
    SK.request.RemoveTagged();

    // An edit might be in progress for the just-deleted item. So
    // now it's not.
    HideGraphicsEditControl();
    SS.TW.HideEditControl();
    // And clear out the selection, which could contain that item.
    ClearSuper();
    // And regenerate to get rid of what it generates, plus anything
    // that references it (since the regen code checks for that).
    SS.GenerateAll(SolveSpaceUI::GENERATE_ALL);
    EnsureValidActives();
    SS.ScheduleShowTW();
}

Vector GraphicsWindow::SnapToGrid(Vector p) {
    if(!LockedInWorkplane()) return p;

    EntityBase *wrkpl = SK.GetEntity(ActiveWorkplane()),
               *norm  = wrkpl->Normal();
    Vector wo = SK.GetEntity(wrkpl->point[0])->PointGetNum(),
           wu = norm->NormalU(),
           wv = norm->NormalV(),
           wn = norm->NormalN();

    Vector pp = (p.Minus(wo)).DotInToCsys(wu, wv, wn);
    pp.x = floor((pp.x / SS.gridSpacing) + 0.5)*SS.gridSpacing;
    pp.y = floor((pp.y / SS.gridSpacing) + 0.5)*SS.gridSpacing;
    pp.z = 0;

    return pp.ScaleOutOfCsys(wu, wv, wn).Plus(wo);
}

void GraphicsWindow::MenuEdit(int id) {
    switch(id) {
        case MNU_UNSELECT_ALL:
            SS.GW.GroupSelection();
            // If there's nothing selected to de-select, and no operation
            // to cancel, then perhaps they want to return to the home
            // screen in the text window.
            if(SS.GW.gs.n               == 0 &&
               SS.GW.gs.constraints     == 0 &&
               SS.GW.pending.operation  == 0)
            {
                if(!(TextEditControlIsVisible() ||
                     GraphicsEditControlIsVisible()))
                {
                    if(SS.TW.shown.screen == TextWindow::SCREEN_STYLE_INFO) {
                        SS.TW.GoToScreen(TextWindow::SCREEN_LIST_OF_STYLES);
                    } else {
                        SS.TW.ClearSuper();
                    }
                }
            }
            SS.GW.ClearSuper();
            SS.TW.HideEditControl();
            SS.nakedEdges.Clear();
            SS.justExportedInfo.draw = false;
            // This clears the marks drawn to indicate which points are
            // still free to drag.
            Param *p;
            for(p = SK.param.First(); p; p = SK.param.NextAfter(p)) {
                p->free = false;
            }
            if(SS.exportMode) {
                SS.exportMode = false;
                SS.GenerateAll(SolveSpaceUI::GENERATE_ALL);
            }
            break;

        case MNU_SELECT_ALL: {
            Entity *e;
            for(e = SK.entity.First(); e; e = SK.entity.NextAfter(e)) {
                if(e->group.v != SS.GW.activeGroup.v) continue;
                if(e->IsFace() || e->IsDistance()) continue;
                if(!e->IsVisible()) continue;

                SS.GW.MakeSelected(e->h);
            }
            InvalidateGraphics();
            SS.ScheduleShowTW();
            break;
        }

        case MNU_SELECT_CHAIN: {
            Entity *e;
            int newlySelected = 0;
            bool didSomething;
            do {
                didSomething = false;
                for(e = SK.entity.First(); e; e = SK.entity.NextAfter(e)) {
                    if(e->group.v != SS.GW.activeGroup.v) continue;
                    if(!e->HasEndpoints()) continue;
                    if(!e->IsVisible()) continue;

                    Vector st = e->EndpointStart(),
                           fi = e->EndpointFinish();

                    bool onChain = false, alreadySelected = false;
                    List<Selection> *ls = &(SS.GW.selection);
                    for(Selection *s = ls->First(); s; s = ls->NextAfter(s)) {
                        if(!s->entity.v) continue;
                        if(s->entity.v == e->h.v) {
                            alreadySelected = true;
                            continue;
                        }
                        Entity *se = SK.GetEntity(s->entity);
                        if(!se->HasEndpoints()) continue;

                        Vector sst = se->EndpointStart(),
                               sfi = se->EndpointFinish();

                        if(sst.Equals(st) || sst.Equals(fi) ||
                           sfi.Equals(st) || sfi.Equals(fi))
                        {
                            onChain = true;
                        }
                    }
                    if(onChain && !alreadySelected) {
                        SS.GW.MakeSelected(e->h);
                        newlySelected++;
                        didSomething = true;
                    }
                }
            } while(didSomething);
            if(newlySelected == 0) {
                Error("没有其他实体与所选实体共享端点。");
            }
            InvalidateGraphics();
            SS.ScheduleShowTW();
            break;
        }

        case MNU_ROTATE_90: {
            SS.GW.GroupSelection();
            Entity *e = NULL;
            if(SS.GW.gs.n == 1 && SS.GW.gs.points == 1) {
                e = SK.GetEntity(SS.GW.gs.point[0]);
            } else if(SS.GW.gs.n == 1 && SS.GW.gs.entities == 1) {
                e = SK.GetEntity(SS.GW.gs.entity[0]);
            }
            SS.GW.ClearSelection();

            hGroup hg = e ? e->group : SS.GW.activeGroup;
            Group *g = SK.GetGroup(hg);
            if(g->type != Group::LINKED) {
                Error("要使用此命令，请从链接零件中选择点或其他图元，或使链接"
                    "组成为激活组。");
                break;
            }


            SS.UndoRemember();
            // Rotate by ninety degrees about the coordinate axis closest
            // to the screen normal.
            Vector norm = SS.GW.projRight.Cross(SS.GW.projUp);
            norm = norm.ClosestOrtho();
            norm = norm.WithMagnitude(1);
            Quaternion qaa = Quaternion::From(norm, PI/2);

            g->TransformImportedBy(Vector::From(0, 0, 0), qaa);

            // and regenerate as necessary.
            SS.MarkGroupDirty(hg);
            SS.GenerateAll();
            break;
        }

        case MNU_SNAP_TO_GRID: {
            if(!SS.GW.LockedInWorkplane()) {
                Error("没有处于活动状态的工作面，选择工作面以定义捕捉网格"
                    "的平面。");
                break;
            }
            SS.GW.GroupSelection();
            if(SS.GW.gs.points == 0 && SS.GW.gs.constraintLabels == 0) {
                Error("无法将这些项目对齐到网格，请使用标签选择点、文本注释或"
                    "约束。要捕捉直线，请选择其端点。");
                break;
            }
            SS.UndoRemember();

            List<Selection> *ls = &(SS.GW.selection);
            for(Selection *s = ls->First(); s; s = ls->NextAfter(s)) {
                if(s->entity.v) {
                    hEntity hp = s->entity;
                    Entity *ep = SK.GetEntity(hp);
                    if(!ep->IsPoint()) continue;

                    Vector p = ep->PointGetNum();
                    ep->PointForceTo(SS.GW.SnapToGrid(p));
                    SS.GW.pending.points.Add(&hp);
                    SS.MarkGroupDirty(ep->group);
                } else if(s->constraint.v) {
                    Constraint *c = SK.GetConstraint(s->constraint);
                    Vector refp = c->GetReferencePos();
                    c->disp.offset = c->disp.offset.Plus(SS.GW.SnapToGrid(refp).Minus(refp));
                }
            }
            // Regenerate, with these points marked as dragged so that they
            // get placed as close as possible to our snap grid.
            SS.GenerateAll();
            SS.GW.ClearPending();

            SS.GW.ClearSelection();
            InvalidateGraphics();
            break;
        }

        case MNU_UNDO:
            SS.UndoUndo();
            break;

        case MNU_REDO:
            SS.UndoRedo();
            break;

        case MNU_REGEN_ALL:
            SS.ReloadAllImported();
            SS.GenerateAll(SolveSpaceUI::GENERATE_UNTIL_ACTIVE);
            SS.ScheduleShowTW();
            break;

        case MNU_EDIT_LINE_STYLES:
            SS.TW.GoToScreen(TextWindow::SCREEN_LIST_OF_STYLES);
            SS.GW.ForceTextWindowShown();
            SS.ScheduleShowTW();
            break;

        case MNU_VIEW_PROJECTION:
            SS.TW.GoToScreen(TextWindow::SCREEN_EDIT_VIEW);
            SS.GW.ForceTextWindowShown();
            SS.ScheduleShowTW();
            break;

        case MNU_CONFIGURATION:
            SS.TW.GoToScreen(TextWindow::SCREEN_CONFIGURATION);
            SS.GW.ForceTextWindowShown();
            SS.ScheduleShowTW();
            break;

        default: oops();
    }
}

void GraphicsWindow::MenuRequest(int id) {
    const char *s;
    switch(id) {
        case MNU_SEL_WORKPLANE: {
            SS.GW.GroupSelection();
            Group *g = SK.GetGroup(SS.GW.activeGroup);

            if(SS.GW.gs.n == 1 && SS.GW.gs.workplanes == 1) {
                // A user-selected workplane
                g->activeWorkplane = SS.GW.gs.entity[0];
            } else if(g->type == Group::DRAWING_WORKPLANE) {
                // The group's default workplane
                g->activeWorkplane = g->h.entity(0);
                Message("未选择工作面，正在激活此组的默认工作面。");
            }

            if(!SS.GW.LockedInWorkplane()) {
                Error("未选择任何工作面，且活动组中没有默认工作面，尝试选"
                    "择工作面，或激活新工作面组中的草图。");
                break;
            }
            // Align the view with the selected workplane
            SS.GW.AnimateOntoWorkplane();
            SS.GW.ClearSuper();
            SS.ScheduleShowTW();
            break;
        }
        case MNU_FREE_IN_3D:
            SS.GW.SetWorkplaneFreeIn3d();
            SS.GW.EnsureValidActives();
            SS.ScheduleShowTW();
            InvalidateGraphics();
            break;

        case MNU_TANGENT_ARC:
            SS.GW.GroupSelection();
            if(SS.GW.gs.n == 1 && SS.GW.gs.points == 1) {
                SS.GW.MakeTangentArc();
            } else if(SS.GW.gs.n != 0) {
                Error("点处的切线圆弧选择错误。选择单个点，或不选择任何内容"
                    "来设置圆弧参数。");
            } else {
                SS.TW.GoToScreen(TextWindow::SCREEN_TANGENT_ARC);
                SS.GW.ForceTextWindowShown();
                SS.ScheduleShowTW();
                InvalidateGraphics(); // repaint toolbar
            }
            break;

        case MNU_ARC: s = "选择圆弧上一个点 (逆时钟绘制)"; goto c;
        case MNU_DATUM_POINT: s = "点击放置基准点"; goto c;
        case MNU_LINE_SEGMENT: s = "点击选择线段的第一点"; goto c;
        case MNU_CONSTR_SEGMENT: s = "点击选择构造线的第一点"; goto c;
        case MNU_CUBIC: s = "点击选择样条曲线的第一点"; goto c;
        case MNU_CIRCLE: s = "点击放置圆心"; goto c;
        case MNU_WORKPLANE: s = "点则选择工作面原点"; goto c;
        case MNU_RECTANGLE: s = "点击选择方形的一个点"; goto c;
        case MNU_TTF_TEXT: s = "点击选择文字左上角"; goto c;
c:
            SS.GW.pending.operation = id;
            SS.GW.pending.description = s;
            SS.ScheduleShowTW();
            InvalidateGraphics(); // repaint toolbar
            break;

        case MNU_CONSTRUCTION: {
            SS.UndoRemember();
            SS.GW.GroupSelection();
            if(SS.GW.gs.entities == 0) {
                Error("未选择任何实体，在尝试切换图元的构造状态之前选取图元。");
            }
            int i;
            for(i = 0; i < SS.GW.gs.entities; i++) {
                hEntity he = SS.GW.gs.entity[i];
                if(!he.isFromRequest()) continue;
                Request *r = SK.GetRequest(he.request());
                r->construction = !(r->construction);
                SS.MarkGroupDirty(r->group);
            }
            SS.GW.ClearSelection();
            SS.GenerateAll();
            break;
        }

        case MNU_SPLIT_CURVES:
            SS.GW.SplitLinesOrCurves();
            break;

        default: oops();
    }
}

void GraphicsWindow::ClearSuper(void) {
    HideGraphicsEditControl();
    ClearPending();
    ClearSelection();
    hover.Clear();
    EnsureValidActives();
}

void GraphicsWindow::ToggleBool(bool *v) {
    *v = !*v;

    // The faces are shown as special stippling on the shaded triangle mesh,
    // so not meaningful to show them and hide the shaded.
    if(!showShaded) showFaces = false;

    // We might need to regenerate the mesh and edge list, since the edges
    // wouldn't have been generated if they were previously hidden.
    if(showEdges) (SK.GetGroup(activeGroup))->displayDirty = true;

    SS.GenerateAll();
    InvalidateGraphics();
    SS.ScheduleShowTW();
}

GraphicsWindow::SuggestedConstraint GraphicsWindow::SuggestLineConstraint(hRequest request) {
    if(LockedInWorkplane()) {
        Entity *ptA = SK.GetEntity(request.entity(1)),
               *ptB = SK.GetEntity(request.entity(2));

        Expr *au, *av, *bu, *bv;

        ptA->PointGetExprsInWorkplane(ActiveWorkplane(), &au, &av);
        ptB->PointGetExprsInWorkplane(ActiveWorkplane(), &bu, &bv);

        double du = au->Minus(bu)->Eval();
        double dv = av->Minus(bv)->Eval();

        const double TOLERANCE_RATIO = 0.02;
        if(fabs(dv) > LENGTH_EPS && fabs(du / dv) < TOLERANCE_RATIO)
            return SUGGESTED_VERTICAL;
        else if(fabs(du) > LENGTH_EPS && fabs(dv / du) < TOLERANCE_RATIO)
            return SUGGESTED_HORIZONTAL;
        else
            return SUGGESTED_NONE;
    } else {
        return SUGGESTED_NONE;
    }
}
