//-----------------------------------------------------------------------------
// The text-based browser window, used to view the structure of the model by
// groups and for other similar purposes.
//
// Copyright 2008-2013 Jonathan Westhues.
//-----------------------------------------------------------------------------
#include "solvespace.h"

//-----------------------------------------------------------------------------
// A navigation bar that always appears at the top of the window, with a
// link to bring us back home.
//-----------------------------------------------------------------------------
void TextWindow::ScreenHome(int link, uint32_t v) {
    SS.TW.GoToScreen(SCREEN_LIST_OF_GROUPS);
}
void TextWindow::ShowHeader(bool withNav) {
    ClearScreen();

    const char *header;
    std::string desc;
    if(SS.GW.LockedInWorkplane()) {
        header = "二维草图：";
        desc = SK.GetEntity(SS.GW.ActiveWorkplane())->DescriptionString();
    } else {
        header = "三维草图";
        desc = "";
    }

    // Navigation buttons
    if(withNav) {
        Printf(false, "%Fl%Lh%f%E %Ft%s%E%s",
            (&TextWindow::ScreenHome), header, desc.c_str());
    } else {
        Printf(false, "%Ft%s%E%s", header, desc.c_str());
    }

    // Leave space for the icons that are painted here.
    Printf(false, "");
    Printf(false, "");
}

//-----------------------------------------------------------------------------
// The screen that shows a list of every group in the sketch, with options
// to hide or show them, and to view them in detail. This is our home page.
//-----------------------------------------------------------------------------
void TextWindow::ScreenSelectGroup(int link, uint32_t v) {
    SS.TW.GoToScreen(SCREEN_GROUP_INFO);
    SS.TW.shown.group.v = v;
}
void TextWindow::ScreenToggleGroupShown(int link, uint32_t v) {
    hGroup hg = { v };
    Group *g = SK.GetGroup(hg);
    g->visible = !(g->visible);
    // If a group was just shown, then it might not have been generated
    // previously, so regenerate.
    SS.GenerateAll();
}
void TextWindow::ScreenShowGroupsSpecial(int link, uint32_t v) {
    bool state = link == 's';
    for(int i = 0; i < SK.groupOrder.n; i++) {
        Group *g = SK.GetGroup(SK.groupOrder.elem[i]);
        g->visible = state;
    }
}
void TextWindow::ScreenActivateGroup(int link, uint32_t v) {
    SS.GW.activeGroup.v = v;
    SK.GetGroup(SS.GW.activeGroup)->Activate();
    SS.GW.ClearSuper();
}
void TextWindow::ReportHowGroupSolved(hGroup hg) {
    SS.GW.ClearSuper();
    SS.TW.GoToScreen(SCREEN_GROUP_SOLVE_INFO);
    SS.TW.shown.group.v = hg.v;
    SS.ScheduleShowTW();
}
void TextWindow::ScreenHowGroupSolved(int link, uint32_t v) {
    if(SS.GW.activeGroup.v != v) {
        ScreenActivateGroup(link, v);
    }
    SS.TW.GoToScreen(SCREEN_GROUP_SOLVE_INFO);
    SS.TW.shown.group.v = v;
}
void TextWindow::ScreenShowConfiguration(int link, uint32_t v) {
    SS.TW.GoToScreen(SCREEN_CONFIGURATION);
}
void TextWindow::ScreenShowEditView(int link, uint32_t v) {
    SS.TW.GoToScreen(SCREEN_EDIT_VIEW);
}
void TextWindow::ScreenGoToWebsite(int link, uint32_t v) {
    OpenWebsite("http://solvespace.com/txtlink");
}
void TextWindow::ShowListOfGroups(void) {
    const char *radioTrue  = " " RADIO_TRUE  " ",
               *radioFalse = " " RADIO_FALSE " ",
               *checkTrue  = " " CHECK_TRUE  " ",
               *checkFalse = " " CHECK_FALSE " ";

    Printf(true, "%Ft 激活");
    Printf(false, "%Ft     可视 检查  组名%E");
    int i;
    bool afterActive = false;
    for(i = 0; i < SK.groupOrder.n; i++) {
        Group *g = SK.GetGroup(SK.groupOrder.elem[i]);
        std::string s = g->DescriptionString();
        bool active = (g->h.v == SS.GW.activeGroup.v);
        bool shown = g->visible;
        bool ok = g->IsSolvedOkay();
        bool warn = (g->type == Group::DRAWING_WORKPLANE &&
                     g->polyError.how != Group::POLY_GOOD) ||
                    ((g->type == Group::EXTRUDE || g->type == Group::LATHE) &&
                     SK.GetGroup(g->opA)->polyError.how != Group::POLY_GOOD);
        int dof = g->solved.dof;
        char sdof[16] = "正常";
        if(ok && dof > 0) {
            if(dof > 999) {
              strcpy(sdof, "###");
            } else {
              sprintf(sdof, "%-4d", dof);
            }
        }
        bool ref = (g->h.v == Group::HGROUP_REFERENCES.v);
        Printf(false, "%Bp%Fd "
               "%Ft%s%Fb%D%f%Ll%s%E "
               "%Fb%s%D%f%Ll%s%E  "
               "%Fp%D%f%s%Ll%s%E  "
               "%Fl%Ll%D%f%s",
               // Alternate between light and dark backgrounds, for readability
               (i & 1) ? 'd' : 'a',
               // Link that activates the group
               ref ? "   " : "",
               g->h.v, (&TextWindow::ScreenActivateGroup),
               ref ? "" : (active ? radioTrue : radioFalse),
               // Link that hides or shows the group
               afterActive ? " - " : "",
               g->h.v, (&TextWindow::ScreenToggleGroupShown),
               afterActive ? "" : (shown ? checkTrue : checkFalse),
               // Link to the errors, if a problem occured while solving
               ok ? (warn ? 'm' : (dof > 0 ? 'i' : 's')) : 'x',
               g->h.v, (&TextWindow::ScreenHowGroupSolved),
               ok ? (warn ? "问题" : sdof) : "",
               ok ? "" : "错误",
               // Link to a screen that gives more details on the group
               g->h.v, (&TextWindow::ScreenSelectGroup), s.c_str());

        if(active) afterActive = true;
    }

    Printf(true,  "  %Fl%Ls%f全部显示%E / %Fl%Lh%f全部隐藏%E",
        &(TextWindow::ScreenShowGroupsSpecial),
        &(TextWindow::ScreenShowGroupsSpecial));
    Printf(true,  "  %Fl%Ls%f线型%E /"
                   " %Fl%Ls%f查看%E /"
                   " %Fl%Ls%f系统设置%E",
        &(TextWindow::ScreenShowListOfStyles),
        &(TextWindow::ScreenShowEditView),
        &(TextWindow::ScreenShowConfiguration));
}


//-----------------------------------------------------------------------------
// The screen that shows information about a specific group, and allows the
// user to edit various things about it.
//-----------------------------------------------------------------------------
void TextWindow::ScreenHoverConstraint(int link, uint32_t v) {
    if(!SS.GW.showConstraints) return;

    hConstraint hc = { v };
    Constraint *c = SK.GetConstraint(hc);
    if(c->group.v != SS.GW.activeGroup.v) {
        // Only constraints in the active group are visible
        return;
    }
    SS.GW.hover.Clear();
    SS.GW.hover.constraint = hc;
    SS.GW.hover.emphasized = true;
}
void TextWindow::ScreenHoverRequest(int link, uint32_t v) {
    SS.GW.hover.Clear();
    hRequest hr = { v };
    SS.GW.hover.entity = hr.entity(0);
    SS.GW.hover.emphasized = true;
}
void TextWindow::ScreenSelectConstraint(int link, uint32_t v) {
    SS.GW.ClearSelection();
    GraphicsWindow::Selection sel = {};
    sel.constraint.v = v;
    SS.GW.selection.Add(&sel);
}
void TextWindow::ScreenSelectRequest(int link, uint32_t v) {
    SS.GW.ClearSelection();
    GraphicsWindow::Selection sel = {};
    hRequest hr = { v };
    sel.entity = hr.entity(0);
    SS.GW.selection.Add(&sel);
}

void TextWindow::ScreenChangeGroupOption(int link, uint32_t v) {
    SS.UndoRemember();
    Group *g = SK.GetGroup(SS.TW.shown.group);

    switch(link) {
        case 's': g->subtype = Group::ONE_SIDED; break;
        case 'S': g->subtype = Group::TWO_SIDED; break;

        case 'k': g->skipFirst = true; break;
        case 'K': g->skipFirst = false; break;

        case 'c':
            if(g->type == Group::EXTRUDE) {
              // When an extrude group is first created, it's positioned for a union
              // extrusion. If no constraints were added, flip it when we switch between
              // union and difference modes to avoid manual work doing the smae.
              if(g->meshCombine != (int)v && g->GetNumConstraints() == 0 &&
                  (v == Group::COMBINE_AS_DIFFERENCE ||
                   g->meshCombine == Group::COMBINE_AS_DIFFERENCE)) {
                g->ExtrusionForceVectorTo(g->ExtrusionGetVector().Negated());
              }
            }
            g->meshCombine = v;
            break;

        case 'P': g->suppress = !(g->suppress); break;

        case 'r': g->relaxConstraints = !(g->relaxConstraints); break;

        case 'e': g->allowRedundant = !(g->allowRedundant); break;

        case 'v': g->visible = !(g->visible); break;

        case 'd': g->allDimsReference = !(g->allDimsReference); break;

        case 'f': g->forceToMesh = !(g->forceToMesh); break;
    }

    SS.MarkGroupDirty(g->h);
    SS.GenerateAll();
    SS.GW.ClearSuper();
}

void TextWindow::ScreenColor(int link, uint32_t v) {
    SS.UndoRemember();

    Group *g = SK.GetGroup(SS.TW.shown.group);
    SS.TW.ShowEditControlWithColorPicker(14, g->color);
    SS.TW.edit.meaning = EDIT_GROUP_COLOR;
}
void TextWindow::ScreenOpacity(int link, uint32_t v) {
    Group *g = SK.GetGroup(SS.TW.shown.group);

    SS.TW.ShowEditControl(12, ssprintf("%.2f", g->color.alphaF()));
    SS.TW.edit.meaning = EDIT_GROUP_OPACITY;
    SS.TW.edit.group.v = g->h.v;
}
void TextWindow::ScreenChangeExprA(int link, uint32_t v) {
    Group *g = SK.GetGroup(SS.TW.shown.group);

    SS.TW.ShowEditControl(10, ssprintf("%d", (int)g->valA));
    SS.TW.edit.meaning = EDIT_TIMES_REPEATED;
    SS.TW.edit.group.v = v;
}
void TextWindow::ScreenChangeGroupName(int link, uint32_t v) {
    Group *g = SK.GetGroup(SS.TW.shown.group);
    SS.TW.ShowEditControl(10, g->DescriptionString().substr(5));
    SS.TW.edit.meaning = EDIT_GROUP_NAME;
    SS.TW.edit.group.v = v;
}
void TextWindow::ScreenChangeGroupScale(int link, uint32_t v) {
    Group *g = SK.GetGroup(SS.TW.shown.group);

    SS.TW.ShowEditControl(13, ssprintf("%.3f", g->scale));
    SS.TW.edit.meaning = EDIT_GROUP_SCALE;
    SS.TW.edit.group.v = v;
}
void TextWindow::ScreenDeleteGroup(int link, uint32_t v) {
    SS.UndoRemember();

    hGroup hg = SS.TW.shown.group;
    if(hg.v == SS.GW.activeGroup.v) {
        SS.GW.activeGroup = SK.GetGroup(SS.GW.activeGroup)->PreviousGroup()->h;
    }

    // Reset the text window, since we're displaying information about
    // the group that's about to get deleted.
    SS.TW.ClearSuper();

    // This is a major change, so let's re-solve everything.
    SK.group.RemoveById(hg);
    SS.GenerateAll(SolveSpaceUI::GENERATE_ALL);

    // Reset the graphics window. This will also recreate the default
    // group if it was removed.
    SS.GW.ClearSuper();
}
void TextWindow::ShowGroupInfo(void) {
    Group *g = SK.GetGroup(shown.group);
    const char *s = "???";

    if(shown.group.v == Group::HGROUP_REFERENCES.v) {
        Printf(true, "%Ft组名 %E%s", g->DescriptionString().c_str());
        goto list_items;
    } else {
        Printf(true, "%Ft组名 %E%s [%Fl%Ll%D%f重名名%E / %Fl%Ll%D%f删除%E]",
            g->DescriptionString().c_str(),
            g->h.v, &TextWindow::ScreenChangeGroupName,
            g->h.v, &TextWindow::ScreenDeleteGroup);
    }

    if(g->type == Group::LATHE) {
        Printf(true, " %Ft旋转成型");
    } else if(g->type == Group::EXTRUDE || g->type == Group::ROTATE ||
              g->type == Group::TRANSLATE)
    {
        if(g->type == Group::EXTRUDE) {
            s = "拉伸成型";
        } else if(g->type == Group::TRANSLATE) {
            s = "位移阵列";
        } else if(g->type == Group::ROTATE) {
            s = "旋转阵列";
        }
        Printf(true, " %Ft%s%E", s);

        bool one = (g->subtype == Group::ONE_SIDED);
        Printf(false,
            "%Ba   %f%Ls%Fd%s 单向%E  "
                  "%f%LS%Fd%s 双向%E",
            &TextWindow::ScreenChangeGroupOption,
            one ? RADIO_TRUE : RADIO_FALSE,
            &TextWindow::ScreenChangeGroupOption,
            !one ? RADIO_TRUE : RADIO_FALSE);

        if(g->type == Group::ROTATE || g->type == Group::TRANSLATE) {
            if(g->subtype == Group::ONE_SIDED) {
                bool skip = g->skipFirst;
                Printf(false,
                   "%Bd   %Ftstart  %f%LK%Fd%s with original%E  "
                         "%f%Lk%Fd%s with copy #1%E",
                    &ScreenChangeGroupOption,
                    !skip ? RADIO_TRUE : RADIO_FALSE,
                    &ScreenChangeGroupOption,
                    skip ? RADIO_TRUE : RADIO_FALSE);
            }

            int times = (int)(g->valA);
            Printf(false, "%Bp   %Ft重复%E %d 次%s %Fl%Ll%D%f[修改]%E",
                (g->subtype == Group::ONE_SIDED) ? 'a' : 'd',
                times, times == 1 ? "" : "s",
                g->h.v, &TextWindow::ScreenChangeExprA);
        }
    } else if(g->type == Group::LINKED) {
        Printf(true, " %Ft外部零件%E");
        Printf(false, "%Ba   '%s'", g->linkFileRel.c_str());
        Printf(false, "%Bd   %Ftscaled by%E %# %Fl%Ll%f%D[修改]%E",
            g->scale,
            &TextWindow::ScreenChangeGroupScale, g->h.v);
    } else if(g->type == Group::DRAWING_3D) {
        Printf(true, " %Ft三维草图%E");
    } else if(g->type == Group::DRAWING_WORKPLANE) {
        Printf(true, " %Ft新工作面草图%E");
    } else {
        Printf(true, "???");
    }
    Printf(false, "");

    if(g->type == Group::EXTRUDE ||
       g->type == Group::LATHE ||
       g->type == Group::LINKED)
    {
        bool un   = (g->meshCombine == Group::COMBINE_AS_UNION);
        bool diff = (g->meshCombine == Group::COMBINE_AS_DIFFERENCE);
        bool asy  = (g->meshCombine == Group::COMBINE_AS_ASSEMBLE);
        bool asa  = (g->type == Group::LINKED);

        Printf(false, " %Ft运算方式");
        Printf(false, "%Ba   %f%D%Lc%Fd%s 并集%E  "
                             "%f%D%Lc%Fd%s 交集%E  "
                             "%f%D%Lc%Fd%s%s%E  ",
            &TextWindow::ScreenChangeGroupOption,
            Group::COMBINE_AS_UNION,
            un ? RADIO_TRUE : RADIO_FALSE,
            &TextWindow::ScreenChangeGroupOption,
            Group::COMBINE_AS_DIFFERENCE,
            diff ? RADIO_TRUE : RADIO_FALSE,
            &TextWindow::ScreenChangeGroupOption,
            Group::COMBINE_AS_ASSEMBLE,
            asa ? (asy ? RADIO_TRUE : RADIO_FALSE) : " ",
            asa ? " 组装" : "");

        if(g->type == Group::EXTRUDE ||
           g->type == Group::LATHE)
        {
            Printf(false,
                "%Bd   %Ft颜色   %E%Bz  %Bd (%@, %@, %@) %f%D%Lf%Fl[修改]%E",
                &g->color,
                g->color.redF(), g->color.greenF(), g->color.blueF(),
                ScreenColor, top[rows-1] + 2);
            Printf(false, "%Bd   %Ft不透明度%E %@ %f%Lf%Fl[修改]%E",
                g->color.alphaF(),
                &TextWindow::ScreenOpacity);
        }

        if (g->type == Group::EXTRUDE ||
            g->type == Group::LATHE ||
            g->type == Group::LINKED) {
            Printf(false, "   %Fd%f%LP%s  抑制此组的实体模型",
                &TextWindow::ScreenChangeGroupOption,
                g->suppress ? CHECK_TRUE : CHECK_FALSE);
        }

        Printf(false, "");
    }

    Printf(false, " %f%Lv%Fd%s  显示此组中的实体",
        &TextWindow::ScreenChangeGroupOption,
        g->visible ? CHECK_TRUE : CHECK_FALSE);

    if(!g->IsForcedToMeshBySource()) {
        Printf(false, " %f%Lf%Fd%s  强制 NURBS 曲面到三角形网格",
            &TextWindow::ScreenChangeGroupOption,
            g->forceToMesh ? CHECK_TRUE : CHECK_FALSE);
    } else {
        Printf(false, " (模型已强制使用三角形网格)");
    }

    Printf(true, " %f%Lr%Fd%s  解除约束限制",
        &TextWindow::ScreenChangeGroupOption,
        g->relaxConstraints ? CHECK_TRUE : CHECK_FALSE);

    Printf(false, " %f%Le%Fd%s  允许冗余约束",
        &TextWindow::ScreenChangeGroupOption,
        g->allowRedundant ? CHECK_TRUE : CHECK_FALSE);

    Printf(false, " %f%Ld%Fd%s  将所有约束视为参考",
        &TextWindow::ScreenChangeGroupOption,
        g->allDimsReference ? CHECK_TRUE : CHECK_FALSE);

    if(g->booleanFailed) {
        Printf(false, "");
        Printf(false, "布尔操作失败。通过选择");
        Printf(false, " \"强制 NURBS 曲面到三角形网格\"");
        Printf(false, "可以解决此问题");
    }

list_items:
    Printf(false, "");
    Printf(false, "%Ft 成员");

    int i, a = 0;
    for(i = 0; i < SK.request.n; i++) {
        Request *r = &(SK.request.elem[i]);

        if(r->group.v == shown.group.v) {
            std::string s = r->DescriptionString();
            Printf(false, "%Bp   %Fl%Ll%D%f%h%s%E",
                (a & 1) ? 'd' : 'a',
                r->h.v, (&TextWindow::ScreenSelectRequest),
                &(TextWindow::ScreenHoverRequest), s.c_str());
            a++;
        }
    }
    if(a == 0) Printf(false, "%Ba   (空)");

    a = 0;
    Printf(false, "");
    Printf(false, "%Ft 约束 (%d 自由度)", g->solved.dof);
    for(i = 0; i < SK.constraint.n; i++) {
        Constraint *c = &(SK.constraint.elem[i]);

        if(c->group.v == shown.group.v) {
            std::string s = c->DescriptionString();
            Printf(false, "%Bp   %Fl%Ll%D%f%h%s%E %s",
                (a & 1) ? 'd' : 'a',
                c->h.v, (&TextWindow::ScreenSelectConstraint),
                (&TextWindow::ScreenHoverConstraint), s.c_str(),
                c->reference ? "(参考)" : "");
            a++;
        }
    }
    if(a == 0) Printf(false, "%Ba   (空)");
}

//-----------------------------------------------------------------------------
// The screen that's displayed when the sketch fails to solve. A report of
// what failed, and (if the problem is a singular Jacobian) a list of
// constraints that could be removed to fix it.
//-----------------------------------------------------------------------------
void TextWindow::ScreenAllowRedundant(int link, uint32_t v) {
    SS.UndoRemember();

    Group *g = SK.GetGroup(SS.TW.shown.group);
    g->allowRedundant = true;
    SS.MarkGroupDirty(g->h);
    SS.GenerateAll();

    SS.TW.shown.screen = SCREEN_GROUP_INFO;
    SS.TW.Show();
}
void TextWindow::ShowGroupSolveInfo(void) {
    Group *g = SK.GetGroup(shown.group);
    if(g->IsSolvedOkay()) {
        // Go back to the default group info screen
        shown.screen = SCREEN_GROUP_INFO;
        Show();
        return;
    }

    Printf(true, "%Ft组名   %E%s", g->DescriptionString().c_str());
    switch(g->solved.how) {
        case System::DIDNT_CONVERGE:
            Printf(true, "%Fx求解失败!%Fd 无法求解的约束");
            Printf(true, "以下约束不兼容");
            break;

        case System::REDUNDANT_DIDNT_CONVERGE:
            Printf(true, "%Fx求解失败!%Fd 无法求解的约束");
            Printf(true, "以下约束未满足");
            break;

        case System::REDUNDANT_OKAY:
            Printf(true, "%Fx求解失败!%Fd 冗余约束");
            Printf(true, "删除其中任何一个来修复它");
            break;

        case System::TOO_MANY_UNKNOWNS:
            Printf(true, "单个组中的未知数过多!");
            return;
    }

    for(int i = 0; i < g->solved.remove.n; i++) {
        hConstraint hc = g->solved.remove.elem[i];
        Constraint *c = SK.constraint.FindByIdNoOops(hc);
        if(!c) continue;

        Printf(false, "%Bp   %Fl%Ll%D%f%h%s%E",
            (i & 1) ? 'd' : 'a',
            c->h.v, (&TextWindow::ScreenSelectConstraint),
            (&TextWindow::ScreenHoverConstraint),
            c->DescriptionString().c_str());
    }

    Printf(true,  "可以通过选择 编辑 -> 撤消 解决此问题。");

    if(g->solved.how == System::REDUNDANT_OKAY) {
      Printf(true,  "可以通过启用 %Fl%f%Ll允许冗余约束%E 来解除此错误.",
          &TextWindow::ScreenAllowRedundant);
    }
}

//-----------------------------------------------------------------------------
// When we're stepping a dimension. User specifies the finish value, and
// how many steps to take in between current and finish, re-solving each
// time.
//-----------------------------------------------------------------------------
void TextWindow::ScreenStepDimFinish(int link, uint32_t v) {
    SS.TW.edit.meaning = EDIT_STEP_DIM_FINISH;
    std::string edit_value;
    if(SS.TW.shown.dimIsDistance) {
        edit_value = SS.MmToString(SS.TW.shown.dimFinish);
    } else {
        edit_value = ssprintf("%.3f", SS.TW.shown.dimFinish);
    }
    SS.TW.ShowEditControl(12, edit_value);
}
void TextWindow::ScreenStepDimSteps(int link, uint32_t v) {
    SS.TW.edit.meaning = EDIT_STEP_DIM_STEPS;
    SS.TW.ShowEditControl(12, ssprintf("%d", SS.TW.shown.dimSteps));
}
void TextWindow::ScreenStepDimGo(int link, uint32_t v) {
    hConstraint hc = SS.TW.shown.constraint;
    Constraint *c = SK.constraint.FindByIdNoOops(hc);
    if(c) {
        SS.UndoRemember();
        double start = c->valA, finish = SS.TW.shown.dimFinish;
        int i, n = SS.TW.shown.dimSteps;
        for(i = 1; i <= n; i++) {
            c = SK.GetConstraint(hc);
            c->valA = start + ((finish - start)*i)/n;
            SS.MarkGroupDirty(c->group);
            SS.GenerateAll();
            if(!SS.ActiveGroupsOkay()) {
                // Failed to solve, so quit
                break;
            }
            PaintGraphics();
        }
    }
    InvalidateGraphics();
    SS.TW.GoToScreen(SCREEN_LIST_OF_GROUPS);
}
void TextWindow::ShowStepDimension(void) {
    Constraint *c = SK.constraint.FindByIdNoOops(shown.constraint);
    if(!c) {
        shown.screen = SCREEN_LIST_OF_GROUPS;
        Show();
        return;
    }

    Printf(true, "%Ft尺寸步进%E %s", c->DescriptionString().c_str());

    if(shown.dimIsDistance) {
        Printf(true,  "%Ba   %Ft开始%E   %s", SS.MmToString(c->valA).c_str());
        Printf(false, "%Bd   %Ft结束%E   %s %Fl%Ll%f[修改]%E",
            SS.MmToString(shown.dimFinish).c_str(), &ScreenStepDimFinish);
    } else {
        Printf(true,  "%Ba   %Ft开始%E   %@", c->valA);
        Printf(false, "%Bd   %Ft结束%E   %@ %Fl%Ll%f[修改]%E",
            shown.dimFinish, &ScreenStepDimFinish);
    }
    Printf(false,     "%Ba   %Ft步距%E   %d %Fl%Ll%f%D[修改]%E",
        shown.dimSteps, &ScreenStepDimSteps);

    Printf(true, " %Fl%Ll%f开始步进%E", &ScreenStepDimGo);

    Printf(true, "(%Fl%Ll%f取消操作%E)", &ScreenHome);
}

//-----------------------------------------------------------------------------
// When we're creating tangent arcs (as requests, not as some parametric
// thing). User gets to specify the radius, and whether the old untrimmed
// curves are kept or deleted.
//-----------------------------------------------------------------------------
void TextWindow::ScreenChangeTangentArc(int link, uint32_t v) {
    switch(link) {
        case 'r': {
            SS.TW.edit.meaning = EDIT_TANGENT_ARC_RADIUS;
            SS.TW.ShowEditControl(3, SS.MmToString(SS.tangentArcRadius));
            break;
        }

        case 'a': SS.tangentArcManual = !SS.tangentArcManual; break;
        case 'm': SS.tangentArcModify = !SS.tangentArcModify; break;
    }
}
void TextWindow::ShowTangentArc(void) {
    Printf(true, "%Ft圆角过渡%E");

    Printf(true,  "%Ft 圆弧半径%E");
    if(SS.tangentArcManual) {
        Printf(false, "%Ba   %s %Fl%Lr%f[修改]%E",
            SS.MmToString(SS.tangentArcRadius).c_str(),
            &(TextWindow::ScreenChangeTangentArc));
    } else {
        Printf(false, "%Ba   自动");
    }

    Printf(false, "");
    Printf(false, "  %Fd%f%La%s  自动选择半径%E",
        &ScreenChangeTangentArc,
        !SS.tangentArcManual ? CHECK_TRUE : CHECK_FALSE);
    Printf(false, "  %Fd%f%Lm%s  修改原始图形%E",
        &ScreenChangeTangentArc,
        SS.tangentArcModify ? CHECK_TRUE : CHECK_FALSE);

    Printf(false, "");
    Printf(false, "要在一个点创建圆角过渡，选择要转换的点，");
    Printf(false, "然后选择 草图 -> 圆角过渡.");
    Printf(true, "(%Fl%Ll%f回到主页%E)", &ScreenHome);
}

//-----------------------------------------------------------------------------
// The edit control is visible, and the user just pressed enter.
//-----------------------------------------------------------------------------
void TextWindow::EditControlDone(const char *s) {
    edit.showAgain = false;

    switch(edit.meaning) {
        case EDIT_TIMES_REPEATED: {
            Expr *e = Expr::From(s, true);
            if(e) {
                SS.UndoRemember();

                double ev = e->Eval();
                if((int)ev < 1) {
                    Error("不能重复少于1次。");
                    break;
                }
                if((int)ev > 999) {
                    Error("不能重复超过999次。");
                    break;
                }

                Group *g = SK.GetGroup(edit.group);
                g->valA = ev;

                if(g->type == Group::ROTATE) {
                    int i, c = 0;
                    for(i = 0; i < SK.constraint.n; i++) {
                        if(SK.constraint.elem[i].group.v == g->h.v) c++;
                    }
                    // If the group does not contain any constraints, then
                    // set the numerical guess to space the copies uniformly
                    // over one rotation. Don't touch the guess if we're
                    // already constrained, because that would break
                    // convergence.
                    if(c == 0) {
                        double copies = (g->skipFirst) ? (ev + 1) : ev;
                        SK.GetParam(g->h.param(3))->val = PI/(2*copies);
                    }
                }

                SS.MarkGroupDirty(g->h);
                SS.ScheduleGenerateAll();
            }
            break;
        }
        case EDIT_GROUP_NAME: {
            if(!*s) {
                Error("组名称不能为空。");
            } else {
                SS.UndoRemember();

                Group *g = SK.GetGroup(edit.group);
                g->name = s;
            }
            break;
        }
        case EDIT_GROUP_SCALE: {
            Expr *e = Expr::From(s, true);
            if(e) {
                double ev = e->Eval();
                if(fabs(ev) < 1e-6) {
                    Error("比例不能为零。");
                } else {
                    Group *g = SK.GetGroup(edit.group);
                    g->scale = ev;
                    SS.MarkGroupDirty(g->h);
                    SS.ScheduleGenerateAll();
                }
            }
            break;
        }
        case EDIT_GROUP_COLOR: {
            Vector rgb;
            if(sscanf(s, "%lf, %lf, %lf", &rgb.x, &rgb.y, &rgb.z)==3) {
                rgb = rgb.ClampWithin(0, 1);

                Group *g = SK.group.FindByIdNoOops(SS.TW.shown.group);
                if(!g) break;
                g->color = RgbaColor::FromFloat(rgb.x, rgb.y, rgb.z,
                    g->color.alphaF());

                SS.MarkGroupDirty(g->h);
                SS.ScheduleGenerateAll();
                SS.GW.ClearSuper();
            } else {
                Error("格式错误：将颜色指定为 r, g, b");
            }
            break;
        }
        case EDIT_GROUP_OPACITY: {
            Expr *e = Expr::From(s, true);
            if(e) {
                double alpha = e->Eval();
                if(alpha < 0 || alpha > 1) {
                    Error("透明度必须介于0和1之间。");
                } else {
                    Group *g = SK.GetGroup(edit.group);
                    g->color.alpha = (int)(255.1f * alpha);
                    SS.MarkGroupDirty(g->h);
                    SS.ScheduleGenerateAll();
                    SS.GW.ClearSuper();
                }
            }
            break;
        }
        case EDIT_TTF_TEXT: {
            SS.UndoRemember();
            Request *r = SK.request.FindByIdNoOops(edit.request);
            if(r) {
                r->str = s;
                SS.MarkGroupDirty(r->group);
                SS.ScheduleGenerateAll();
            }
            break;
        }
        case EDIT_STEP_DIM_FINISH: {
            Expr *e = Expr::From(s, true);
            if(!e) {
                break;
            }
            if(shown.dimIsDistance) {
                shown.dimFinish = SS.ExprToMm(e);
            } else {
                shown.dimFinish = e->Eval();
            }
            break;
        }
        case EDIT_STEP_DIM_STEPS:
            shown.dimSteps = min(300, max(1, atoi(s)));
            break;

        case EDIT_TANGENT_ARC_RADIUS: {
            Expr *e = Expr::From(s, true);
            if(!e) break;
            if(e->Eval() < LENGTH_EPS) {
                Error("半径不能为零或负数。");
                break;
            }
            SS.tangentArcRadius = SS.ExprToMm(e);
            break;
        }

        default: {
            int cnt = 0;
            if(EditControlDoneForStyles(s))         cnt++;
            if(EditControlDoneForConfiguration(s))  cnt++;
            if(EditControlDoneForPaste(s))          cnt++;
            if(EditControlDoneForView(s))           cnt++;
            if(cnt > 1) {
                // The identifiers were somehow assigned not uniquely?
                oops();
            }
            break;
        }
    }
    InvalidateGraphics();
    SS.ScheduleShowTW();

    if(!edit.showAgain) {
        HideEditControl();
        edit.meaning = EDIT_NOTHING;
    }
}

