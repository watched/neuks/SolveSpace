############################################################################### 
# General Purpose Microsoft Makefile
# 
# Copyright (C) 2018, Martin Tang
############################################################################### 

# Toolchain
AR=$(TARGET)ar
PP=$(TARGET)cpp
AS=$(TARGET)as
CC=$(TARGET)gcc
CX=$(TARGET)g++
DB=$(TARGET)gdb
OD=$(TARGET)objdump
OC=$(TARGET)objcopy
RC=windres
FL=flex
BS=bison
LD=$(TARGET)g++
RM=rm

# Configuration
ARFLAGS=cr
PPFLAGS=-Isrc \
				-Iextlib/libdxfrw \
				-Iextlib/libpng \
				-Iextlib/libfreetype\include \
				-Iextlib/libz \
				-DWIN32 \
				-DWIN32_IE=_WIN32_WINNT \
				-DWIN32_WINNT=0x500 \
				-DWIN32_LEAN_AND_MEAN \
				-DUNICODE \
				-D_UNICODE \
				-DNOMINMAX \
				-DISOLATION_AWARE_ENABLED
ASFLAGS=$(PPFLAGS)
CCFLAGS=$(PPFLAGS) \
				-O3 \
				-m32 \
				-std=c11 \
				-march=native \
				-mtune=native \
				-mfpmath=sse \
				-msse4
CXFLAGS=$(PPFLAGS) \
				-O3 \
				-m32 \
				-std=c++11 \
				-march=native \
				-mtune=native \
				-mfpmath=sse \
				-msse4
DBFLAGS=
RCFLAGS=-Fpe-i386
FLFLAGS=
BSFLAGS=
LDFLAGS=-m32 \
				-static \
				-Lextlib/libdxfrw \
				-Lextlib/libpng \
				-Lextlib/libfreetype \
				-Lextlib/libz \
				-mwindows
EXFLAGS=

# Projects
OBJECT1=src/win32/resource.o \
				src/win32/w32main.o \
				src/win32/w32util.o \
				src/bsp.o \
				src/clipboard.o \
				src/confscreen.o \
				src/constraint.o \
				src/constrainteq.o \
				src/describescreen.o \
				src/draw.o \
				src/drawconstraint.o \
				src/drawentity.o \
				src/entity.o \
				src/export.o \
				src/exportstep.o \
				src/exportvector.o \
				src/expr.o \
				src/file.o \
				src/generate.o \
				src/glhelper.o \
				src/graphicswin.o \
				src/group.o \
				src/groupmesh.o \
				src/importdxf.o \
				src/mesh.o \
				src/modify.o \
				src/mouse.o \
				src/polygon.o \
				src/request.o \
				src/solvespace.o \
				src/style.o \
				src/system.o \
				src/textscreens.o \
				src/textwin.o \
				src/toolbar.o \
				src/ttf.o \
				src/undoredo.o \
				src/util.o \
				src/view.o \
				src/generated/icons.o \
				src/srf/boolean.o \
				src/srf/curve.o \
				src/srf/merge.o \
				src/srf/ratpoly.o \
				src/srf/raycast.o \
				src/srf/surface.o \
				src/srf/surfinter.o \
				src/srf/triangulate.o
OUTPUT1=SolveSpace.exe

# Summary
HEADERS=src/generated/icons.h \
				src/generated/icons.cpp \
				src/generated/bitmapfont.table.h \
				src/generated/vectorfont.table.h
LIBRARY=-lcomctl32 -lcomdlg32 -lopengl32 -lglu32 -lgdi32 \
				-lpng -lz -lfreetype -ldxfrw
EXTOOLS=png2c.exe lff2c.exe unifont2c.exe
OBJECTS=$(OBJECT1)
OUTPUTS=$(OUTPUT1)
DEPENDS=$(OBJECTS:.o=.dep)
CLEANUP=$(OBJECTS) $(OUTPUTS) $(HEADERS) $(DEPENDS)

# Build Commands
all: $(OUTPUTS)
coc: compile_commands.json

$(OUTPUT1) : $(OBJECT1)

libs:
	@make -C extlib/libdxfrw
	@make -C extlib/libfreetype
	@make -C extlib/libpng
	@make -C extlib/libz

$(EXTOOLS): libs
	make -C src/tools all

src/generated/icons.cpp src/generated/icons.h: png2c.exe
	@echo [PNG2C] icons.cpp icons.h
	@png2c src/generated/icons.cpp src/generated/icons.h \
		icons/angle.png \
		icons/arc.png \
		icons/assemble.png \
		icons/bezier.png \
		icons/circle.png \
		icons/constraint.png \
		icons/construction.png \
		icons/edges.png \
		icons/equal.png \
		icons/extrude.png \
		icons/faces.png \
		icons/hidden-lines.png \
		icons/horiz.png \
		icons/in3d.png \
		icons/lathe.png \
		icons/length.png \
		icons/line.png \
		icons/mesh.png \
		icons/normal.png \
		icons/ontoworkplane.png \
		icons/other-supp.png \
		icons/outlines.png \
		icons/parallel.png \
		icons/perpendicular.png \
		icons/point.png \
		icons/pointonx.png \
		icons/rectangle.png \
		icons/ref.png \
		icons/same-orientation.png \
		icons/shaded.png \
		icons/sketch-in-3d.png \
		icons/sketch-in-plane.png \
		icons/step-rotate.png \
		icons/step-translate.png \
		icons/symmetric.png \
		icons/tangent-arc.png \
		icons/text.png \
		icons/trim.png \
		icons/vert.png \
		icons/workplane.png

src/textwin.o: src/generated/icons.h

src/generated/bitmapfont.table.h: unifont2c.exe
	@echo [UNIFONT2C] bitmapfont.table.h
	@unifont2c src/generated/bitmapfont.table.h \
		fonts/unifont-8.0.01.hex.gz \
		fonts/private/0-check-false.png \
		fonts/private/1-check-true.png \
		fonts/private/2-radio-false.png \
		fonts/private/3-radio-true.png \
		fonts/private/4-stipple-dot.png \
		fonts/private/5-stipple-dash-long.png \
		fonts/private/6-stipple-dash.png \
		fonts/private/7-stipple-zigzag.png
	
src/generated/vectorfont.table.h: lff2c.exe
	@echo [LFF2C] vectorfont.table.h
	@lff2c src/generated/vectorfont.table.h fonts/unicode.lff.gz

src/glhelper.o: src/generated/vectorfont.table.h \
	src/generated/bitmapfont.table.h

run: $(OUTPUT1)
	@echo [EX] $<
	@$< $(EXFLAGS)

debug: $(OUTPUTS)
	@echo [DB] $<
	@$(DB) $(DBFLAGS) $<

clean:
	@make -C extlib/libdxfrw clean
	@make -C extlib/libfreetype clean
	@make -C extlib/libpng clean
	@make -C extlib/libz clean
	@make -C src/tools clean
	@echo [RM] $(CLEANUP)
	-@$(RM) $(CLEANUP)

# Standard Procedures
%.dep : %.S
	@$(PP) $(PPFLAGS) -MM -MT $(@:.dep=.o) -o $@ $<

%.dep : %.c
	@$(PP) $(PPFLAGS) -MM -MT $(@:.dep=.o) -o $@ $<

%.dep : %.cpp
	@$(PP) $(PPFLAGS) -MM -MT $(@:.dep=.o) -o $@ $<

%.dep : %.rc
	@$(PP) $(PPFLAGS) -MM -MT $(@:.dep=.o) -o $@ $<

%.dep : %.l
	@$(PP) $(PPFLAGS) -MM -MT $(@:.dep=.o) -o $@ $<

%.dep : %.y
	@$(PP) $(PPFLAGS) -MM -MT $(@:.dep=.o) -o $@ $<

%.o : %.S
	@echo [AS] $<
	@$(AS) $(ASFLAGS) -c -o $@ $<

%.o : %.c
	@echo [CC] $<
	@$(CC) $(CCFLAGS) -c -o $@ $<

%.o : %.cpp
	@echo [CX] $<
	@$(CX) $(CXFLAGS) -c -o $@ $<

%.o : %.rc
	@echo [RC] $<
	@$(RC) $(RCFLAGS) $< $@ 

%.c : %.l
	@echo [FL] $<
	@$(FL) $(FLFLAGS) -o $@ $<

%.c : %.y
	@echo [BS] $<
	@$(BS) $(BSFLAGS) -d -o $@ $<

%.a :
	@echo [AR] $@
	@$(AR) $(ARFLAGS) $@ $^

%.dll :
	@echo [LD] $@
	@$(LD) $(LDFLAGS) -o $@ $^ $(LIBRARY)

%.exe :
	@echo [LD] $@
	@$(LD) $(LDFLAGS) -o $@ $^ $(LIBRARY)

%.elf:
	@echo [LD] $@
	@$(LD) $(LDFLAGS) -o $@ $^ $(LIBRARY)

%.hex : %.exe
	@echo [OC] $@
	@$(OC) -O ihex $< $@

%.hex : %.elf
	@echo [OC] $@
	@$(OC) -O ihex $< $@

ifneq ($(MAKECMDGOALS),clean)
-include $(DEPENDS)
endif
